use anyhow::Result;
use std::path::PathBuf;

use crate::utils;

pub fn process(uboot: utils::FilesArray, root: &PathBuf) -> Result<()> {
    let mut settings_dir = PathBuf::from("/");
    settings_dir.push("etc/mobile-tweaks/u-boot");

    let mut uboot_dir = PathBuf::from(root);
    uboot_dir.push("etc/u-boot-menu.d");

    let links = utils::filelist_to_links(&uboot, &settings_dir, &uboot_dir);
    utils::link_files(&links)?;

    utils::execute("/usr/sbin/u-boot-update", None)?;

    Ok(())
}
