use std::path::PathBuf;

use crate::utils;
use anyhow::Result;

pub fn process(default: utils::FilesArray, root: &PathBuf) -> Result<()> {
    let mut settings_dir = PathBuf::from("/");
    settings_dir.push("etc/mobile-tweaks/default");

    let mut default_dir = PathBuf::from(root);
    default_dir.push("etc/default");

    let links = utils::filelist_to_links(&default, &settings_dir, &default_dir);
    utils::link_files(&links)
}
