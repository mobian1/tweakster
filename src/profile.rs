use anyhow::Result;
use std::path::PathBuf;

use crate::utils;

pub fn process(profile: utils::FilesArray, root: &PathBuf) -> Result<()> {
    let mut settings_dir = PathBuf::from("/");
    settings_dir.push("etc/mobile-tweaks/profile");

    let mut profile_dir = PathBuf::from(root);
    profile_dir.push("etc/profile.d");

    let links = utils::filelist_to_links(&profile, &settings_dir, &profile_dir);
    utils::link_files(&links)
}
