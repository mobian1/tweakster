use anyhow::Result;
use merge::Merge;
use serde::Deserialize;
use std::path::PathBuf;

use crate::utils;

#[derive(Deserialize, Debug, Default, Clone)]
struct Script {
    stage: String,
    name: String,
}

#[derive(Deserialize, Debug, Default, Clone, Merge)]
pub struct Config {
    hooks: Option<Vec<String>>,
    scripts: Option<Vec<Script>>,
}

fn process_scripts(
    scripts: Vec<Script>,
    origin: &PathBuf,
    initramfs_dir: &PathBuf,
) -> Vec<utils::Link> {
    let mut origin_dir = PathBuf::from(&origin);
    origin_dir.push("scripts");

    let mut destination_dir = PathBuf::from(&initramfs_dir);
    destination_dir.push("scripts");

    let mut links: Vec<utils::Link> = Vec::with_capacity(scripts.len());

    for script in scripts {
        let mut link = utils::Link {
            origin_dir: PathBuf::from(&origin_dir),
            origin_file: PathBuf::from(&script.name),
            destination_dir: PathBuf::from(&destination_dir),
            destination_file: PathBuf::from(&script.name),
        };

        link.origin_dir.push(&script.stage);
        link.destination_dir.push(&script.stage);

        links.push(link);
    }

    links
}

pub fn process(initramfs: Config, root: &PathBuf) -> Result<()> {
    let mut settings_dir = PathBuf::from("/");
    settings_dir.push("etc/mobile-tweaks/initramfs");

    let mut initramfs_dir = PathBuf::from(root);
    initramfs_dir.push("etc/initramfs-tools");

    let mut links: Vec<utils::Link> = Vec::new();

    if let Some(hooks) = initramfs.hooks {
        let mut hooks_orig = PathBuf::from(&settings_dir);
        hooks_orig.push("hooks");

        let mut hooks_dest = PathBuf::from(&initramfs_dir);
        hooks_dest.push("hooks");

        links.extend(utils::stringlist_to_links(&hooks, &hooks_orig, &hooks_dest));
    }

    if let Some(scripts) = initramfs.scripts {
        links.extend(process_scripts(scripts, &settings_dir, &initramfs_dir));
    }

    utils::link_files(&links)?;
    utils::execute("/usr/sbin/update-initramfs", Some(vec!["-u", "-k", "all"]))?;

    Ok(())
}
