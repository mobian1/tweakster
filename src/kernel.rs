use anyhow::Result;
use merge::Merge;
use serde::Deserialize;
use std::path::PathBuf;

use crate::utils;

#[derive(Deserialize, Debug, Default, Clone)]
struct Script {
    name: String,
    prefix: Option<String>,
}

#[derive(Deserialize, Debug, Default, Clone, Merge)]
pub struct Config {
    preinst: Option<Vec<Script>>,
    prerm: Option<Vec<Script>>,
    postinst: Option<Vec<Script>>,
    postrm: Option<Vec<Script>>,
}

fn process_stage(
    scripts: Vec<Script>,
    origin_dir: &PathBuf,
    kscripts_dir: &PathBuf,
    subdir: &str,
) -> Vec<utils::Link> {
    let mut orig_dir = PathBuf::from(origin_dir);
    orig_dir.push(subdir);

    let mut dest_dir = PathBuf::from(kscripts_dir);
    dest_dir.push(subdir);

    let mut links: Vec<utils::Link> = Vec::with_capacity(scripts.len());

    for script in scripts {
        let mut dest_name = script.name.clone();

        if let Some(prefix) = script.prefix {
            dest_name = format!("{}-{}", prefix, script.name);
        }

        links.push(utils::Link {
            origin_dir: PathBuf::from(&orig_dir),
            origin_file: PathBuf::from(script.name),
            destination_dir: PathBuf::from(&dest_dir),
            destination_file: PathBuf::from(dest_name),
        });
    }

    links
}

pub fn process(kernel: Config, root: &PathBuf) -> Result<()> {
    let mut settings_dir = PathBuf::from("/");
    settings_dir.push("etc/mobile-tweaks/kernel");

    let mut kernel_dir = PathBuf::from(root);
    kernel_dir.push("etc/kernel");

    let mut links: Vec<utils::Link> = Vec::new();

    if let Some(preinst) = kernel.preinst {
        links.extend(process_stage(
            preinst,
            &settings_dir,
            &kernel_dir,
            "preinst.d",
        ));
    }
    if let Some(prerm) = kernel.prerm {
        links.extend(process_stage(prerm, &settings_dir, &kernel_dir, "prerm.d"));
    }
    if let Some(postinst) = kernel.postinst {
        links.extend(process_stage(
            postinst,
            &settings_dir,
            &kernel_dir,
            "postinst.d",
        ));
    }
    if let Some(postrm) = kernel.postrm {
        links.extend(process_stage(
            postrm,
            &settings_dir,
            &kernel_dir,
            "postrm.d",
        ));
    }

    utils::link_files(&links)
}
